// We require the jsonwebtoken module and then contain it in jtw variable.
const jwt = require('jsonwebtoken');


// USED IN ALGORITHM for encrypting our data which, makes it difficult to decode the information without defined secret key.

const secret = "CourseBookingAPI";


/*Section JSON Web token */

// Token creation
/*
    Analogy:
        Pack the gift provided with a lock, which can only be opened using the secrret code as the key.

*/

module.exports.createAccessToken = (user) =>{
    // payload of the JWT
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    // Generate a JSON a web token using the jwt's sign method.
        // Syntax:
            // jwt.sign(payload, secretOrPrivateKey, [callbackfunction])
    
   return jwt.sign(data, secret, {});

}


/*Token verification */
/*
-Analogy
    receive the gift and open the lock to verify if the sender is legitimate and the gift was not tampered with.

*/

module.exports.verify = (req, res, next) =>{
        let token = req.headers.authorization;
        console.log(token);

        if(token !== undefined ){
        // Validate the "token" using verify method, to decrypt the token using the secret code.
        /* Syntax:
            jwt.verify(token, secret, [callback function])   */
            token = token.slice(7, token.length);
            console.log("Token without bearer",token)
            return jwt.verify(token, secret, (error,data)=>{
                if(error){
                    return res.send("Invalid Token")
                }
                else{
                    next();
                }
            })
        }

        else{
            res.send("Authentication failed! No token provided")
        }
}


// Token decryption
/*
    -Analogy:
        unwrapping of gift presents
*/

module.exports.decode = (token) => {
    if (token === undefined){
        return null 
    }
    else{
        token = token.slice(7, token.length);
        return jwt.verify(token, secret, (error, data)=>{
            if (error){
                return null;
            }
            else{
                // decode method is used to obtain the information from the JWT.
                // Syntax: jwt.decode (token, [options])
                // Return an object with the access to the payload property.
                return jwt.decode(token, {complete:true}).payload
            }
        })
    }

}