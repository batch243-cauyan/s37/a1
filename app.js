require('dotenv').config()

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// allow access to routes defined within our application
const userRoutes = require('./routes/userRoutes')
const courseRoutes = require('./routes/courseRoutes')
const app = express();
const port = process.env.PORT || 5000;

// Middlewares. Allows all resources to access our backend application.
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" to be included for all user routes defined in the userRoutes route file.
app.use('/users',userRoutes)

// Defines the "/courses" to be included for all course routes defined in the courseRoutes route file.
app.use('/courses', courseRoutes)


mongoose.connect(process.env.myDBURI, {
    useNewUrlParser: true,
    useUnifiedTopology:true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=>console.log("Connected to cloud database."));


// This syntax will allow flexibility when using the application both locally or on a server.
// process.env.PORT

app.listen(port, ()=>{
    console.log(`API is now online on port ${port}`);
})

// JWT : header , payload, signature