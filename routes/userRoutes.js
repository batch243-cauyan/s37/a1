const express = require('express');

const router = express.Router();
const userController = require('../controllers/userControllers')
const auth = require('../auth')
const courseController = require('../controllers/courseControllers')

// checkEmail
router.post('/checkEmail', userController.checkEmailExists)
    // res.status(200).json({msg:"welcome"})

// registerin new user
router.post('/register', userController.checkEmailExists, userController.registerUser)

// login
router.post(`/login`, userController.loginUser)

//GET  Users
router.get('/',userController.getAllUser)

// access token gained by logging provides access to other userDetails
//GET  One user by ID, provide _id and token
router.post('/details',auth.verify, userController.findUser)


// provide access token and this will return the respective user's profile
router.get('/profile', userController.profileDetails);


module.exports = router;