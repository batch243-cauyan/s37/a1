

const mongoose = require('mongoose');

// Create the Schema using the mongoose.Schema() function
const coursesSchema = new mongoose.Schema({
    name: {
        type: String,
        required:[true, "Course is required"]
    },
    description: {
        type: String,
        required:[true, "Course is required"]
    },
    price: {
        type: Number,
        required: [true,"Price is required"]
    },
    slots: {
        type: Number,
        required:[true, "Number of Slots is required"]
    },
    isActive:{
        type:Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    
    enrollees: [
        {
            userID: {
                type: String,
                required: [true, "UserID is required"]
                    },
            
            enrolledOn: {
                type: Date,
                default: new Date()
                        }
        }
    ]
});





module.exports = mongoose.model("Course",coursesSchema);