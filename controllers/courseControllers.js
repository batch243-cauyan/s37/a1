const Course = require ('../models/courses');
const User = require('../models/users');
const router = require('../routes/userRoutes');
// const mongoose = require('mongoose')
const bcrypt = require('bcrypt');

const auth = require('../auth');




module.exports.getAllCourses = async (req,res)=>{

    await Course.find({}).sort({name:1})
    .then(result=>
        {return res.status(200).send(result)}
        )
    .catch(error=>{
        res.status(400).send(`No users found.`)
    })
}


/*
    Steps:
        1.Create a new Course object using mongoose model and information from the request of the user.
        2.Save the new Course the database. 

*/

module.exports.addCourse = (req,res)=>{
    const userData = auth.decode(req.headers.authorization);
    
    console.log(userData)

    let newCourse = new Course ({

        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        slots: req.body.slots
        
      })
  
    if (userData.isAdmin === true)
      {
    return newCourse.save()
    .then(course=>{
        console.log(course)
        res.send(true)
    })
    .catch(err=>{
        console.log(err)
        res.send(false);
    })}
    else{
        return res.send(`You don't have admin privileges`)
    }
}