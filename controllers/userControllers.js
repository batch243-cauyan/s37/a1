const Course = require ('../models/courses.js');
const User = require('../models/users');
const router = require('../routes/userRoutes');
// const mongoose = require('mongoose')
const bcrypt = require('bcrypt');

const auth = require('../auth');
// Check if email already exists
/*
    Steps:
    1. Use mongoose "find" method to find duplicate emails.
    2. Use the "then" mthod to send a response to the frontend application based on the result of the find method.

*/


module.exports.checkEmailExists = (req, res, next)=>{
    // The result is sent back to the frontend via the then method
    return User.find({email:req.body.email}).then(result=>{
        let message = "";
            // find method returns an array record of matching documents
        if(result.length > 0){
            message = `The ${req.body.email} is already taken, please use other email.`
            return res.status(200).send(message)
        }
            // No duplicate email found
            // The email is not yet registered in the database.
        else{
            // message = `That the email: ${req.body.email} is not yet taken.`
            // return res.status(200).send(message)
             next();
        }
    })
}


module.exports.registerUser =  (req,res) =>{
    // creates varaible "newUser" and instantiates a new 'User' object using mongoose model
      let newUser = new User ({
        firstName: req.body.firstName,
        lastName : req.body.lastName,
        email : req.body.email,
        // password:req.body.password,
        // salt - salt rounds that bcrypt algorithm will run to encrypt the password
        password: bcrypt.hashSync (req.body.password, 10),
        mobileNo: req.body.mobileNo
      })

      return  newUser.save()
      .then(user=>res.status(200).send(`Congratulations, Sir/Ma'am ${newUser.firstName}!. You are now registered.`))
      .catch(error=>{
        console.log(error);
        res.send(`Sorry ${newUser.firstName}, there was an error during the registration. Please Try again!`)
      })

 
}


module.exports.getAllUser = async (req,res)=>{

    await User.find({}).sort({lastName:1})
    .then(result=>
        {return res.status(200).send(result)}
        )
    .catch(error=>{
        res.status(400).send(`No users found.`)
    })
}

// User Authentication  
/*
    Steps:
        1. Check database if the user email exists.
        2. compare the password provided in the login form with the password stored in the database.


*/


// Start of LOGIN 
module.exports.loginUser = (req,res) =>{
    // The findOne method, returns the first record in the collection that matches the search criteria.

    // if(!req.body.email === null) 
    return User.findOne({email:req.body.email})
    .then((user => {
        if (user === null){
            return res.send(`Your email: ${req.body.email} is not yet registered. Register first!`) 
        }
        else{
            //  compares db hashed/encrypted password with the entered password of the user
            // The compareSync method is used to compare a non encrypted password from the login to the encrypted password retrieved. It will return true or false value depending on the result
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, user.password);

            if(isPasswordCorrect){
                return res.send({accessToken: auth.createAccessToken(user)})
                // return res.send(`Logged in successfully!`)
            }

                return res.send(`Incorrect password, please try again!`)
        }

    }))

}
// END of LOGIN


module.exports.findUser = async (req,res)=>{

    await User.findById({_id: req.body.id})
    .then(result=>
        {
            result.password = "";
            
            return res.status(200).send(result)}
        )
    .catch(error=>{
        console.log(error)
        res.status(400).send(`No users found.`)
    })
}



module.exports.profileDetails = (req,res) =>{
    // user will be object that contains the id and email of the user that is currently logged in.
    const userData = auth.decode(req.headers.authorization);
    console.log(userData);
    return User.findById(userData.id).then(result=>{
        result.password = "Confidential";
        return res.send(result)
    }).catch(err => {
        return res.send(err);
    })
}